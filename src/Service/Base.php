<?php


namespace App\Service;

use App\Entity\Stats;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class Base extends AbstractController
{
    /**
     * @var Request
     */
    private $request;
    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var CacheInterface
     */
    private $cache;

    public function __construct(RequestStack $requestStack, ValidatorInterface $validator, CacheInterface $cache)
    {
        $this->request = $requestStack->getCurrentRequest();
        $this->validator = $validator;
        $this->cache = $cache;
    }

    public function getData($delete = false, $getUser = true)
    {
        if($delete){
            $this->cache->delete('dataService');
        }
        $request = $this->request;
        $validator = $this->validator;

        $appUser = $this->get('security.token_storage')->getToken()->getUser();
        if ($appUser !== "anon." AND $appUser->getRole() !== "ROLE_ADMIN") {
            $stats = new Stats();
            $stats->setCreatedAt(new \DateTime())
                ->setUri($request->getUri());
            if (count($validator->validate($stats)) == 0) {
                $stats = $this->getDoctrine()->getRepository(Stats::class)->saveStats($stats);
            }
        } else if ($appUser == "anon.") {
            $stats = new Stats();
            $stats->setCreatedAt(new \DateTime())
                ->setUri($request->getUri());
            if (count($validator->validate($stats)) == 0) {
                $this->getDoctrine()->getRepository(Stats::class)->saveStats($stats);
            }
        }
        $data = $this->cache->get('dataService', function (ItemInterface $item) use ($appUser, $getUser) {
            $item->expiresAfter(604800);
        });

        return array(
            "data" => $data,
            "appUser" => $appUser,
            "url" => "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
            "baseUri" => "https://" . $_SERVER['HTTP_HOST']
        );
    }
}