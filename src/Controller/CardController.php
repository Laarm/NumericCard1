<?php

namespace App\Controller;

use App\Service\Base;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CardController extends AbstractController
{
    /**
     * @Route("/c/{id}/{name}", name="card")
     * @param Base $base
     * @return Response
     */
    public function index(Base $base): Response
    {
        $data = $base->getData();
        return $this->render('card/index.html.twig', [
            "data" => $data,
        ]);
    }
}
