<?php

namespace App\Repository;

use App\Entity\Stats;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Stats|null find($id, $lockMode = null, $lockVersion = null)
 * @method Stats|null findOneBy(array $criteria, array $orderBy = null)
 * @method Stats[]    findAll()
 * @method Stats[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Stats::class);
    }

    public function saveStats($stats)
    {
        $this->_em->persist($stats);
        $this->_em->flush();
        return $stats->getId();
    }

    public function getStats() {
        $query = $this->createQueryBuilder('a');
        return $query
            ->select("a")
            ->groupBy("a.uri")
            ->getQuery()
            ->getResult();
    }

    public function getStatsBy($uri) {
        $query = $this->createQueryBuilder('a');
        return $query
            ->select('count(a.id)')
            ->where('a.uri LIKE :uri')
            ->setParameter('uri', '%' . $uri . '%')
            ->distinct()
            ->getQuery()
            ->getResult();
    }

    public function getStatsCount() {
        $query = $this->createQueryBuilder('a');
        return $query
            ->select('count(a)')
            ->getQuery()
            ->getResult();
    }
}
