<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class LoginController extends AbstractController
{
    /**
     * @Route("/login", name="login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function index(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        return $this->render('login/index.html.twig', [
            'error'    => $error,
        ]);
    }

    /**
     * @Route("/signup", name="signup")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function signup(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        return $this->render('login/signup.html.twig', [
            'error'    => $error,
        ]);
    }

    /**
     * @Route("/passwordForgot", name="passwordForgot")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function passwordForgot(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        return $this->render('login/index.html.twig', [
            'error'    => $error,
        ]);
    }


    /**
     * @Route("/ajax/signUp", name="ajaxSignUp")
     * @param ValidatorInterface $validator
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param Request $request
     * @return Response
     */
    public function ajaxSignUp(ValidatorInterface $validator, UserPasswordEncoderInterface $passwordEncoder, Request $request): Response
    {
        if ($this->isCsrfTokenValid('create-user-signup', $request->get('csrfData')) && strlen($request->get('password')) > 6) {
            $sqlUser = new User();
            $sqlUser->setFirstname($request->get('firstname'))
                ->setLastname($request->get('lastname'))
                ->setUsername(strtolower(substr($request->get('firstname'), 0, 5).substr($request->get('lastname'),0,5).rand(10000, 99999)))
                ->setPassword($passwordEncoder->encodePassword($sqlUser, $request->get('password')))
                ->setEmail($request->get('email'))
                ->setCreatedAt(new \DateTime());
            if(count($validator->validate($sqlUser)) == 0) {
                $this->getDoctrine()->getRepository(User::class)->createUser($sqlUser);
                return $this->json(['message' => 'Vous êtes bien inscrit !'], 200);
            }
            return $this->json(['message' => $validator->validate($sqlUser)], 400);
        }
        return $this->json(['message' => ['detail' => 'Votre mot de passe est trop court !']], 400);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout()
    {
    }
}
