<?php

namespace App\Controller;

use App\Entity\Card;
use App\Entity\Theme;
use App\Repository\CardRepository;
use App\Repository\StatsRepository;
use App\Repository\ThemeRepository;
use App\Service\Base;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin/dashboard", name="adminDashboard")
     * @param Base $base
     * @param CardRepository $cardRepository
     * @param ThemeRepository $themeRepository
     * @param StatsRepository $statsRepository
     * @param ValidatorInterface $validator
     * @return Response
     */
    public function index(Base $base, CardRepository $cardRepository,ThemeRepository $themeRepository, StatsRepository $statsRepository, ValidatorInterface $validator): Response
    {
        $data = $base->getData();
        if(!$cardRepository->findBy(array('createdBy' => $data['appUser']))) {
            $card = new Card();
            $card->setName($data['appUser']->getFirstName()."_".$data['appUser']->getLastName())
                ->setFirstname($data['appUser']->getFirstName())
                ->setLastname($data['appUser']->getLastName())
                ->setCreatedBy($data['appUser'])
                ->setDescription("Ceci est la description...")
                ->setPhone("06 00 00 00 00")
                ->setEmail($data['appUser']->getEmail())
                ->setWebsite("https://numericcard.com/c/".$data['appUser']->getFirstName()."_".$data['appUser']->getLastName())
                ->setAdress("1 avenue de l'exemple")
                ->setTheme($themeRepository->find(1))
                ->setCreatedAt(new \DateTime());
            if (count($validator->validate($card)) == 0) {
                $themeRepository->createTheme($card);
            }
        }
        $card = $cardRepository->findBy(array("createdBy" => $data['appUser']));
        $countView = $statsRepository->findBy(array("uri" => $data['appUser']));
        return $this->render('admin/index.html.twig', [
            "card" => $card,
            "data" => $data
        ]);
    }
}
